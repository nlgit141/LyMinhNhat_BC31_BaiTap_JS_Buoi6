/*bài tập 1: Tìm số nguyên dương nhỏ nhất
 */
var numberValue = document.getElementById("txt-number").value * 1;
var sum = 0;
for (var index = 0; sum < 10000; index++) {
    sum = sum + index;
    if (sum > 10000) {
        console.log("số nguyên dương nhỏ nhất là:", index);
    }
}

function timSoNlonNhat() {
    var numberValue = document.getElementById("txt-number").value * 1;
    var sum = 0;
    for (var index = 1; sum < numberValue; index++) {
        sum = sum + index;
        if (sum > numberValue) {
            console.log("số nguyên dương nhỏ nhất là:", index);
        }
        document.getElementById(
            "ket-qua-so-N"
        ).innerHTML = `<div>số nguyên dương nhỏ nhất là: ${index}</div>`;
    }
}

/** Bài tập 2:
 * input: Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2 + x^3 + … + x^n (Sử dụng vòng lặp và hàm)
 *
 * áp dụng công thức:
 * công thức: Math.pow(x,n)
 * hoặc x**n
 * sum
 *
 *
 * output: tính tổng
 */
function tinhTong() {
    var xValue = document.getElementById("txt-number-x").value * 1;
    var nValue = document.getElementById("txt-number-n").value * 1;
    var luyThua = 0;
    var sum = 0;
    for (var i = 1; i <= nValue; i++) {
        console.log(xValue, "mũ", i);
        luyThua = Math.pow(xValue, i);
        sum = sum + luyThua;
    }
    console.log("tổng =", sum);
    document.getElementById(
        "ket-qua-tong"
    ).innerHTML = `<div>Kết quả = ${sum}</div>`;
}

/** BÀI 3:Nhập vào n. Tính giai thừa 1*2*...n
 
 * 
 */
function tinhGiaiThua() {
    var giaiThuaValue = document.getElementById("txt-giaithua").value * 1;
    var nValue = 1;
    for (var i = 1; i <= giaiThuaValue; i++) {
        nValue = nValue * i;
    }
    console.log("số giai thừa: ", nValue);
    document.getElementById(
        "ketqua-giaithua"
    ).innerHTML = `<div>số giai thừa: ${nValue}</div>`;
}

/**BÀI 4: Click vào button in ra 10 thẻ div có background đỏ là chẵn xanh là lẻ
 *
 */

function taoDivChanLe() {
    var chanLe = 0;
    var result = "";
    for (chanLe = 1; chanLe <= 10; chanLe++) {
        if (chanLe % 2 == 0) {
            result += "<div style='background-color: red; color:white'>" + "div chẵn " + chanLe + "</div>";
        } else {
            result += "<div style='background-color: blue; color:white'>" + "div lẻ " + chanLe + "</div>";
        }
    }
    document.getElementById("ket-qua-chan-le").innerHTML = result;
}
